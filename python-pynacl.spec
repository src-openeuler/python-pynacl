%{?python_enable_dependency_generator}
%bcond_without check

Name:          python-pynacl
Summary:       Python binding to the libsodium library
Version:       1.5.0
Release:       1
License:       ASL 2.0
URL:           https://github.com/pyca/pynacl
Source0:       https://github.com/pyca/pynacl/archive/%{version}/pynacl-%{version}.tar.gz

BuildRequires: gcc libsodium-devel

%description
PyNaCl is a Python binding to libsodium, which is a fork of the Networking and Cryptography library.
These libraries have a stated goal of improving usability, security and speed. It supports Python 2.7
and 3.4+ as well as PyPy 2.6+.

%package       -n python3-pynacl
Summary:       Python binding to the libsodium library

BuildRequires: python3-devel python3-setuptools python3-cffi >= 1.4.1
BuildRequires: python3-six python3-pytest >= 3.2.1 python3-hypothesis >= 3.27.0
BuildRequires: python3-wheel
%{?python_provide:%python_provide python3-pynacl}

%description   -n python3-pynacl
PyNaCl is a Python binding to libsodium, which is a fork of the Networking and Cryptography library.
These libraries have a stated goal of improving usability, security and speed. python3-pynacl supports
Python 3.4+ as well as PyPy 2.6+.

%prep
%autosetup -n pynacl-%{version}
rm -vrf src/libsodium/

%build
export SODIUM_INSTALL=system
%py3_build

%install
%py3_install

%check
%if %{with check}
PYTHONPATH=%{buildroot}%{python3_sitearch} py.test-3 -v
%endif

%files -n python3-pynacl
%license LICENSE
%doc README.rst
%{python3_sitearch}/PyNaCl-*.egg-info/
%{python3_sitearch}/nacl/

%changelog
* Fri Jul 22 2022 liks11@chinaunicom.cn - 1.5.0-1
- update package to 1.5.0

* Mon Apr 11 2022 zhanzhimin<zhanzhimin@h.partners.com> - 1.4.0-3
- enable tests

* Wed Aug 5 2020 yanglongkang <yanglongkang@huawei.com> - 1.4.0-2
- fix build fail,delete python2 buildrequire

* Thu Jul 16 2020 jixinjie <jixinjie@huawei.com> - 1.4.0-1
- update package to 1.4.0

* Wed Nov 13 2019 Lijin Yang <yanglijin@huawei.com> - 1.2.1-4
- init package
